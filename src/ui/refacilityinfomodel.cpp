#include "refacilityinfomodel.h"

REFacilityInfoModel::REFacilityInfoModel(QObject *parent)
    : QAbstractTableModel(parent),
      m_collector(new REFacilityInfoCollector(Config::FromQSettings(), this)),
      m_items(),
      m_infosCollected(0) {
  connect(m_collector.get(), &REFacilityInfoCollector::authorized, this,
          &REFacilityInfoModel::collectorAuthorized);
  connect(m_collector.get(), &REFacilityInfoCollector::collected, this,
          &REFacilityInfoModel::collectorCollected);
  connect(m_collector.get(), &REFacilityInfoCollector::error, this,
          &REFacilityInfoModel::collectorError);
}

const QVector<REFacilityInfo> REFacilityInfoModel::getItems() const {
  return m_items;
}

void REFacilityInfoModel::setItems(const QVector<REFacilityInfo> &items) {
  beginResetModel();

  m_items = items;

  endResetModel();
}

int REFacilityInfoModel::rowCount(const QModelIndex & /*parent*/) const {
  return m_items.size();
}

int REFacilityInfoModel::columnCount(const QModelIndex & /*parent*/) const {
  return ColumnCount;
}

QVariant REFacilityInfoModel::headerData(int section,
                                         Qt::Orientation orientation,
                                         int role) const {
  if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
    switch (section) {
      case ColumnSheetTitle:
        return QString("Страница");
      case ColumnINN:
        return QString("ИНН");
      case ColumnOGRN:
        return QString("ОГРН");
      case ColumnSeries:
        return QString("Серия");
      case ColumnNumber:
        return QString("Номер");
      case ColumnType:
        return QString("Тип");
      case ColumnStatus:
        return QString("Статус");
      case ColumnActiveFrom:
        return QString("Активна с");
      case ColumnActiveTo:
        return QString("Активна до");
      case ColumnDateOfExclusion:
        return QString("Дата исключения");
    }
  }

  return QVariant();
}

QVariant REFacilityInfoModel::data(const QModelIndex &index, int role) const {
  if (role != Qt::DisplayRole) {
    return QVariant();
  }

  const auto &item = m_items[index.row()];

  switch (index.column()) {
    case ColumnSheetTitle:
      return item.SheetTitle;
    case ColumnINN:
      return item.INN;
    case ColumnOGRN:
      return item.OGRN;
    case ColumnSeries:
      return item.Series;
    case ColumnNumber:
      return item.Number;
    case ColumnType:
      return item.Type;
    case ColumnStatus:
      return item.Status;
    case ColumnActiveFrom:
      return item.ActiveFromDate;
    case ColumnActiveTo:
      return item.ActiveToDate;
    case ColumnDateOfExclusion:
      return item.DateOfExclusion;
  }

  return QVariant();
}

void REFacilityInfoModel::collectInfo() {
  emit collectInfoStarted(rowCount(index(0, 0)));

  m_infosCollected = 0;

  m_collector->authorize();
}

void REFacilityInfoModel::collectorAuthorized() {
  for (qsizetype i = 0; i < m_items.size(); i++) {
    const auto &item = m_items[i];

    m_collector->collect(item, i);
  }
}

void REFacilityInfoModel::collectorCollected(REFacilityID id,
                                             REFacilityInfo info) {
  auto &item = m_items[id];
  item = info;

  emit dataChanged(index(id, 0), index(id, ColumnCount - 1), {Qt::DisplayRole});

  incrementProgress();
}

void REFacilityInfoModel::collectorError(REFacilityID id,
                                         REFacilityInfoError error) {
  switch (error.Type) {
    case REFacilityInfoError::Authorization: {
      emit collectInfoFailed(error.Message);
      break;
    }

    case REFacilityInfoError::Collect: {
      auto &item = m_items[id];
      item.Status = error.Message;

      emit dataChanged(index(id, ColumnStatus), index(id, ColumnStatus),
                       {Qt::DisplayRole});

      incrementProgress();

      break;
    }

    default: {
      break;
    }
  }
}

bool REFacilityInfoModel::incrementProgress() {
  m_infosCollected += 1;

  if (m_infosCollected >= rowCount(index(0, 0))) {
    emit collectInfoFinished();

    return true;  // finished
  }

  emit collectInfoProgress(m_infosCollected);

  return false;
}