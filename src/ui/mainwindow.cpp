#include "mainwindow.h"

#include <QDate>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include <QTime>

#include "../xlsx.h"
#include "settingsdialog.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      m_ui(new Ui::MainWindow),
      m_reFacilityInfoModel(new REFacilityInfoModel(this)) {
  m_ui->setupUi(this);

  m_ui->progressBar->setHidden(true);
  m_ui->tableView->setModel(m_reFacilityInfoModel.get());

  connect(m_ui->openFileAction, &QAction::triggered, this,
          &MainWindow::openFileTriggered);
  connect(m_ui->saveFileAction, &QAction::triggered, this,
          &MainWindow::saveFileTriggered);
  connect(m_ui->openSettingsDialogAction, &QAction::triggered, this,
          &MainWindow::openSettingsDialogTriggered);
  connect(m_ui->runButton, &QPushButton::pressed, this,
          &MainWindow::runPressed);

  connect(m_reFacilityInfoModel.get(), &REFacilityInfoModel::collectInfoStarted,
          this, [this](int total) {
            lockControls(true);
            m_ui->progressBar->setValue(0);
            m_ui->progressBar->setMaximum(total);
          });

  connect(m_reFacilityInfoModel.get(),
          &REFacilityInfoModel::collectInfoProgress, this,
          [this](int progress) { m_ui->progressBar->setValue(progress); });

  connect(m_reFacilityInfoModel.get(),
          &REFacilityInfoModel::collectInfoFinished, this,
          [this]() { lockControls(false); });

  connect(m_reFacilityInfoModel.get(), &REFacilityInfoModel::collectInfoFailed,
          this, [this](QString reason) {
            lockControls(false);
            QMessageBox::critical(this, "Ошибка", reason);
          });
}

MainWindow::~MainWindow() {}

void MainWindow::openFileTriggered() {
  auto config = Config::FromQSettings();

  auto fileName = QFileDialog::getOpenFileName(this, "Открыть файл",
                                               config.WorkDir, "XLSX (*.xlsx)");
  if (fileName == "") {
    return;
  }

  QFileInfo fileInfo(fileName);

  auto absoluteFilePath = fileInfo.absoluteFilePath();

  m_ui->fileName->setText(absoluteFilePath);

  m_reFacilityInfoModel->setItems(xlsx::parseREFacilityInfo(absoluteFilePath));

  config.WorkDir = fileInfo.dir().path();
  config.save();
}

void MainWindow::saveFileTriggered() {
  QFileInfo fileInfo(m_ui->fileName->text());

  auto baseName = fileInfo.baseName();
  if (baseName == "") {
    baseName = "ret";
  }

  auto dir = fileInfo.dir();
  auto currentDate = QDate::currentDate();
  auto currentTime = QTime::currentTime();

  QString suggestedFileName =
      dir.filePath(QString("%1_%2_%3.xlsx")
                       .arg(baseName, currentDate.toString("ddMMyy"),
                            currentTime.toString("hhmmss")));

  auto fileName = QFileDialog::getSaveFileName(
      this, "Сохранить файл", suggestedFileName, "XLSX (*.xlsx)");
  if (fileName == "") {
    return;
  }

  xlsx::writeREFacilityInfo(m_reFacilityInfoModel->getItems(), fileName);

  QMessageBox::information(this, "Файл сохранён", "Файл успешно сохранён");
}

void MainWindow::openSettingsDialogTriggered() {
  SettingsDialog dialog(this);
  dialog.exec();
}

void MainWindow::runPressed() { m_reFacilityInfoModel->collectInfo(); }

void MainWindow::lockControls(bool lock) {
  m_ui->runButton->setDisabled(lock);
  m_ui->openFileButton->setDisabled(lock);
  m_ui->saveFileButton->setDisabled(lock);
  m_ui->openFileAction->setDisabled(lock);
  m_ui->progressBar->setVisible(lock);
}
