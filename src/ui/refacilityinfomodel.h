#ifndef REFacilityInfoModel_H
#define REFacilityInfoModel_H

#include <QAbstractTableModel>
#include <QVector>

#include "../refacilityinfo.h"
#include "../refacilityinfocollector.h"

class REFacilityInfoModel : public QAbstractTableModel {
  Q_OBJECT

 public:
  enum Columns {
    ColumnSheetTitle = 0,
    ColumnSeries,
    ColumnNumber,
    ColumnINN,
    ColumnOGRN,
    ColumnType,
    ColumnStatus,
    ColumnActiveFrom,
    ColumnActiveTo,
    ColumnDateOfExclusion,
    ColumnCount
  };

 signals:
  void collectInfoStarted(int total);
  void collectInfoProgress(int progress);
  void collectInfoFinished();
  void collectInfoFailed(QString reason);

 private:
  std::unique_ptr<REFacilityInfoCollector> m_collector;

  QVector<REFacilityInfo> m_items;
  REFacilityID m_infosCollected;

 public:
  REFacilityInfoModel(QObject *parent = nullptr);

  const QVector<REFacilityInfo> getItems() const;
  void setItems(const QVector<REFacilityInfo> &items);

  int rowCount(const QModelIndex & /*parent*/) const;
  int columnCount(const QModelIndex & /*parent*/) const;

  QVariant headerData(int section, Qt::Orientation orientation, int role) const;
  QVariant data(const QModelIndex &index, int role) const;

 public slots:
  void collectInfo();

 private slots:
  void collectorAuthorized();
  void collectorCollected(REFacilityID id, REFacilityInfo info);
  void collectorError(REFacilityID id, REFacilityInfoError error);

 private:
  bool incrementProgress();
};

#endif  // REFacilityInfoModel_H