#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>

#include "refacilityinfomodel.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 private:
  std::unique_ptr<Ui::MainWindow> m_ui;
  std::unique_ptr<REFacilityInfoModel> m_reFacilityInfoModel;

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 private slots:
  void openFileTriggered();
  void saveFileTriggered();
  void openSettingsDialogTriggered();
  void runPressed();
  void lockControls(bool lock);
};

#endif  // MAINWINDOW_H
