#include "settingsdialog.h"

#include "../config.h"
#include "ui_settingsdialog.h"

#define PRESET_

SettingsDialog::SettingsDialog(QWidget *parent)
    : QDialog(parent), m_ui(new Ui::SettingsDialog) {
  m_ui->setupUi(this);

  auto config = Config::FromQSettings();

  auto workDir = config.WorkDir;

  m_ui->presetInput->addItem("МТС", PresetMTS);
  m_ui->presetInput->addItem("МГТС", PresetMGTS);
  m_ui->presetInput->addItem("МВС", PresetMVS);

  switch (config.Preset) {
    case PresetMTS:
    case PresetMGTS:
    case PresetMVS:
      m_ui->presetInput->setCurrentIndex(
          m_ui->presetInput->findData(config.Preset));
      m_ui->innInput->setText(config.getINN());
      m_ui->ogrnInput->setText(config.getOGRN());
      break;
    default:
      // explicit set to default (custom disabled at the moment)
      m_ui->presetInput->setCurrentIndex(
          m_ui->presetInput->findData(DefaultPreset));
      m_ui->innInput->setText(DefaultINN);
      m_ui->ogrnInput->setText(DefaultOGRN);
  }

  m_ui->urlInput->setText(config.CheckURL);
  m_ui->innInput->setText(config.CustomINN);
  m_ui->ogrnInput->setText(config.CustomOGRN);

  // http proxy
  m_ui->httpProxy->setChecked(config.HTTPProxy.InUse);
  m_ui->httpProxyHostInput->setText(config.HTTPProxy.Host);
  m_ui->httpProxyPortInput->setValue(config.HTTPProxy.Port);
  m_ui->httpProxyLoginInput->setText(config.HTTPProxy.User);
  m_ui->httpProxyPasswordInput->setText(config.HTTPProxy.Password);

  connect(m_ui->presetInput, &QComboBox::activated, m_ui->presetInput,
          [this](int index) {
            auto preset = m_ui->presetInput->itemData(index).toInt();

            switch (preset) {
              case PresetMTS:
                m_ui->innInput->setText(MTSPresetINN);
                m_ui->ogrnInput->setText(MTSPresetOGRN);
                break;
              case PresetMGTS:
                m_ui->innInput->setText(MGTSPresetINN);
                m_ui->ogrnInput->setText(MGTSPresetOGRN);
                break;
              case PresetMVS:
                m_ui->innInput->setText(MVSPresetINN);
                m_ui->ogrnInput->setText(MVSPresetOGRN);
                break;
              default:
                m_ui->innInput->setText(DefaultINN);
                m_ui->ogrnInput->setText(DefaultOGRN);
            }
          });

  connect(this, &SettingsDialog::accepted, this, [this, config]() {
    auto intPreset = m_ui->presetInput->currentData().toInt();

    Config newConfig{m_ui->urlInput->text(),
                     static_cast<enum Preset>(intPreset),
                     m_ui->innInput->text(),
                     m_ui->ogrnInput->text(),
                     config.WorkDir,
                     HTTPProxyConfig{
                         m_ui->httpProxy->isChecked(),
                         m_ui->httpProxyHostInput->text(),
                         m_ui->httpProxyPortInput->value(),
                         m_ui->httpProxyLoginInput->text(),
                         m_ui->httpProxyPasswordInput->text(),
                     }};

    newConfig.save();
  });
}

SettingsDialog::~SettingsDialog() {}
