#ifndef REFACILITYINFO_H
#define REFACILITYINFO_H

#include <QMetaType>
#include <QString>

#include "refacility.h"

struct REFacilityInfo : REFacility {
  QString Type;
  QString Status;

  QString ActiveFromDate;
  QString ActiveToDate;
  QString DateOfExclusion;
};

Q_DECLARE_METATYPE(REFacilityInfo);

#endif  // REFACILITYINFO_H
