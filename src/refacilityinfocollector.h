#ifndef RKNAPI_H
#define RKNAPI_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QObject>
#include <QRegularExpression>
#include <memory>

#include "config.h"
#include "refacilityinfo.h"

struct REFacilityCollectorContext {
  bool IsAuthorization;

  REFacility REFacility;
  REFacilityID ID;
};

struct REFacilityInfoError {
  enum ErrorType { None, Authorization, Collect } Type;

  QString Message;
};

class REFacilityInfoCollector : public QObject {
  Q_OBJECT

 public:
  enum Attribute { ContextAttribute = QNetworkRequest::User };

 signals:
  void authorized();
  void collected(REFacilityID, REFacilityInfo);
  void error(REFacilityID, REFacilityInfoError);

 private:
  Config m_config;
  QString m_token;

  std::unique_ptr<QNetworkAccessManager> m_manager;

  QRegularExpression m_tokenRX;
  QRegularExpression m_infoRowRX;

 public:
  REFacilityInfoCollector(QObject *parent = nullptr);
  REFacilityInfoCollector(const Config &config, QObject *parent = nullptr);
  ~REFacilityInfoCollector();

  void setConfig(const Config &config);
  void authorize();
  void collect(REFacility reFacility, REFacilityID id = -1);
  void loadFromXLSX(const QString &fname);

 private:
  void raiseError(const REFacilityInfoError &error, REFacilityID id = -1);
  void authorizeRequestFinished(QNetworkReply *reply);
  void collectRequestFinished(const REFacilityCollectorContext &context,
                              QNetworkReply *reply);

 private slots:
  void requestFinished(QNetworkReply *reply);
};

#endif  // RKNAPI_H
