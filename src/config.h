#ifndef CONFIG_H
#define CONFIG_H

#include <QString>

const char *const OrgName = "nshibalov";
const char *const OrgDomain = "nshibalov.com";
const char *const AppName = "qt-rrefac";

const char *const SettingsCheckURL = "rrefac/check_url";
const char *const SettingsPreset = "rrefac/preset";
const char *const SettingsCustomINN = "rrefac/custom_inn";
const char *const SettingsCustomOGRN = "rrefac/custom_ogrn";
const char *const SettingsWorkDir = "rrefac/work_dir";

const char *const SettingsHTTPProxyInUse = "http_proxy/in_use";
const char *const SettingsHTTPProxyHost = "http_proxy/host";
const char *const SettingsHTTPProxyPort = "http_proxy/port";
const char *const SettingsHTTPProxyUser = "http_proxy/user";
const char *const SettingsHTTPProxyPassword = "http_proxy/password";

const char *const DefaultCheckURL =
    "https://rkn.gov.ru/communication/register/registerRes/";

enum Preset { PresetCustom = 0, PresetMTS, PresetMGTS, PresetMVS };

const char *const MTSPresetINN = "7740000076";
const char *const MTSPresetOGRN = "1027700149124";

const char *const MGTSPresetINN = "7710016640";
const char *const MGTSPresetOGRN = "1027739285265";

const char *const MVSPresetINN = "9709073534";
const char *const MVSPresetOGRN = "1217700367026";

const Preset DefaultPreset = PresetMTS;
const char *const DefaultINN = MTSPresetINN;
const char *const DefaultOGRN = MTSPresetOGRN;

struct HTTPProxyConfig {
  bool InUse;
  QString Host;
  int Port;
  QString User;
  QString Password;
};

struct Config {
  QString CheckURL;
  Preset Preset;
  QString CustomINN;
  QString CustomOGRN;
  QString WorkDir;

  HTTPProxyConfig HTTPProxy;

  static Config Default();
  static Config FromQSettings();

  QString getINN();
  QString getOGRN();

  void save();

 private:
  void applyProxy();
};

#endif  // CONFIG_H
