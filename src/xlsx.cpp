#include "xlsx.h"

#include <QMap>
#include <xlnt/xlnt.hpp>

namespace {

enum XLSXReadColumn {
  XLSXReadColumnSeries = 0,
  XLSXReadColumnNumber,
  XLSXReadColumnCount
};

enum XLSXWriteColumn {
  XLSXWriteColumnSeries = 0,
  XLSXWriteColumnNumber,
  XLSXWriteColumnINN,
  XLSXWriteColumnOGRN,
  XLSXWriteColumnType,
  XLSXWriteColumnStatus,
  XLSXWriteColumnActiveFrom,
  XLSXWriteColumnActiveTo,
  XLSXWriteColumnDateOfExclusion
};

QString rCell(xlnt::range_iterator::reference row, XLSXReadColumn column) {
  return QString::fromStdString(row[column].to_string());
}

void wCell(xlnt::worksheet &ws, xlnt::row_t row, XLSXWriteColumn column,
           const QString &value) {
  ws.cell(xlnt::cell_reference(column + 1, row + 1)).value(value.toStdString());
}

void parseREFacilityInfoWorkSheet(xlnt::worksheet ws, int rowOffset,
                                  QVector<REFacilityInfo> &ret) {
  auto title = QString::fromStdString(ws.title());

  for (auto row : ws.rows(true)) {
    if (rowOffset > 0) {
      --rowOffset;
      continue;
    }

    if (row.length() < XLSXReadColumnCount) {
      continue;
    }

    auto reFacility = REFacilityInfo{};

    reFacility.SheetTitle = title;
    reFacility.Series = rCell(row, XLSXReadColumnSeries);
    reFacility.Number = rCell(row, XLSXReadColumnNumber);

    ret.append(reFacility);
  }
}

};  // namespace

namespace xlsx {

QVector<REFacilityInfo> parseREFacilityInfo(const QString &fileName,
                                            int rowOffset) {
  xlnt::workbook wb;
  wb.load(fileName.toStdString());

  xlnt::row_t row_count = 0;

  for (size_t i = 0; i < wb.sheet_count(); ++i) {
    auto ws = wb.sheet_by_index(i);

    row_count += ws.highest_row();
  }

  auto ret = QVector<REFacilityInfo>();
  ret.reserve(row_count);

  for (size_t i = 0; i < wb.sheet_count(); ++i) {
    auto ws = wb.sheet_by_index(i);

    parseREFacilityInfoWorkSheet(ws, rowOffset, ret);
  }

  return ret;
}

void writeREFacilityInfo(const QVector<REFacilityInfo> &infos,
                         const QString &fileName) {
  QMap<QString, xlnt::worksheet> worksheets;

  xlnt::workbook wb;
  wb.remove_sheet(wb.active_sheet());

  for (auto &info : infos) {
    xlnt::worksheet ws;

    if (!worksheets.contains(info.SheetTitle)) {
      ws = wb.create_sheet(worksheets.size());
      ws.title(info.SheetTitle.toStdString());

      worksheets[info.SheetTitle] = ws;

      auto row = 0;

      wCell(ws, row, XLSXWriteColumnSeries, "Серия");
      wCell(ws, row, XLSXWriteColumnNumber, "Номер");
      wCell(ws, row, XLSXWriteColumnINN, "ИНН");
      wCell(ws, row, XLSXWriteColumnOGRN, "ОГРН");
      wCell(ws, row, XLSXWriteColumnType, "Тип");
      wCell(ws, row, XLSXWriteColumnStatus, "Статус");
      wCell(ws, row, XLSXWriteColumnActiveFrom, "Активна с");
      wCell(ws, row, XLSXWriteColumnActiveTo, "Активна до");
      wCell(ws, row, XLSXWriteColumnDateOfExclusion, "Дата исключения");
    } else {
      ws = worksheets[info.SheetTitle];
    }

    auto row = ws.highest_row();

    wCell(ws, row, XLSXWriteColumnSeries, info.Series);
    wCell(ws, row, XLSXWriteColumnNumber, info.Number);
    wCell(ws, row, XLSXWriteColumnINN, info.INN);
    wCell(ws, row, XLSXWriteColumnOGRN, info.OGRN);
    wCell(ws, row, XLSXWriteColumnType, info.Type);
    wCell(ws, row, XLSXWriteColumnStatus, info.Status);
    wCell(ws, row, XLSXWriteColumnActiveFrom, info.ActiveFromDate);
    wCell(ws, row, XLSXWriteColumnActiveTo, info.ActiveToDate);
    wCell(ws, row, XLSXWriteColumnDateOfExclusion, info.DateOfExclusion);
  }

  wb.save(fileName.toStdString());
}

}  // namespace xlsx