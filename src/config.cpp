#include "config.h"

#include <QNetworkProxy>
#include <QSettings>

Config Config::Default() {
  return Config{
      DefaultCheckURL,  // CheckURL
      DefaultPreset,    // Preset
      DefaultINN,       // INN
      DefaultOGRN,      // OGRN
  };
}

Config Config::FromQSettings() {
  QSettings settings;

  auto intPreset = settings.value(SettingsPreset, DefaultPreset).toInt();

  auto config = Config{
      settings.value(SettingsCheckURL, DefaultCheckURL).toString(),  // CheckURL
      static_cast<enum Preset>(intPreset),                           // Preset
      settings.value(SettingsCustomINN, "").toString(),              // INN
      settings.value(SettingsCustomOGRN, "").toString(),             // OGRN
      settings.value(SettingsWorkDir, "").toString(),                // WorkDir
      HTTPProxyConfig{
          settings.value(SettingsHTTPProxyInUse, false).toBool(),
          settings.value(SettingsHTTPProxyHost, "").toString(),
          settings.value(SettingsHTTPProxyPort, 80).toInt(),
          settings.value(SettingsHTTPProxyUser, "").toString(),
          settings.value(SettingsHTTPProxyPassword, "").toString(),
      }};

  config.applyProxy();

  return config;
}

QString Config::getINN() {
  switch (Preset) {
    case PresetCustom:
      return CustomINN;
    case PresetMTS:
      return MTSPresetINN;
    case PresetMGTS:
      return MGTSPresetINN;
    case PresetMVS:
      return MVSPresetINN;
  }

  return DefaultINN;
}

QString Config::getOGRN() {
  switch (Preset) {
    case PresetCustom:
      return CustomOGRN;
    case PresetMTS:
      return MTSPresetOGRN;
    case PresetMGTS:
      return MGTSPresetOGRN;
    case PresetMVS:
      return MVSPresetOGRN;
  }

  return DefaultOGRN;
}

void Config::save() {
  QSettings settings;

  settings.setValue(SettingsCheckURL, CheckURL);
  settings.setValue(SettingsPreset, Preset);
  settings.setValue(SettingsCustomINN, CustomINN);
  settings.setValue(SettingsCustomOGRN, CustomOGRN);
  settings.setValue(SettingsWorkDir, WorkDir);

  settings.setValue(SettingsHTTPProxyInUse, HTTPProxy.InUse);
  settings.setValue(SettingsHTTPProxyHost, HTTPProxy.Host);
  settings.setValue(SettingsHTTPProxyPort, HTTPProxy.Port);
  settings.setValue(SettingsHTTPProxyUser, HTTPProxy.User);
  settings.setValue(SettingsHTTPProxyPassword, HTTPProxy.Password);

  applyProxy();
}

void Config::applyProxy() {
  if (HTTPProxy.InUse) {
    QNetworkProxy proxy;
    proxy.setType(QNetworkProxy::HttpProxy);
    proxy.setHostName(HTTPProxy.Host);
    proxy.setPort(HTTPProxy.Port);
    proxy.setUser(HTTPProxy.User);
    proxy.setPassword(HTTPProxy.Password);
    QNetworkProxy::setApplicationProxy(proxy);
  } else {
    QNetworkProxy proxy;
    proxy.setType(QNetworkProxy::DefaultProxy);
    QNetworkProxy::setApplicationProxy(proxy);
  }
}
