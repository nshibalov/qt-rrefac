#ifndef REFACILITY_H
#define REFACILITY_H

#include <QString>

typedef int REFacilityID;

struct REFacility {
  QString SheetTitle;

  QString INN;
  QString OGRN;
  QString Series;
  QString Number;
};

#endif  // REFACILITY_H
