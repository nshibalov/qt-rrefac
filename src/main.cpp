#include <QApplication>

#include "config.h"
#include "ui/mainwindow.h"

int main(int argc, char *argv[]) {
  QCoreApplication::setOrganizationName(OrgName);
  QCoreApplication::setOrganizationDomain(OrgDomain);
  QCoreApplication::setApplicationName(AppName);

  QApplication a(argc, argv);

  MainWindow w;
  w.show();

  return a.exec();
}
