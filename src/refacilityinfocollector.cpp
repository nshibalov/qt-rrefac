#include "refacilityinfocollector.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QRegularExpression>
#include <QUrlQuery>

const char *const TableParamType = "Тип РЭС по ЕТС";
const char *const TableParamStatus = "Статус записи";
const char *const TableParamActiveFrom = "Дата начала действия";
const char *const TableParamActiveTo = "Дата окончания действия";
const char *const TableParamDateOfExclusion = "Дата исключения";

const char *const QueryArgINN = "inn";
const char *const QueryArgOGRN = "ogrn";
const char *const QueryArgSerial = "seria";
const char *const QueryArgNumber = "num";
const char *const QueryArgToken = "csrftoken";

const char *const JsonResponseKeyMessage = "msg";
const char *const JsonResponseKeyHtml = "html";

const char *const ContentTypeFormUrlEncoded =
    "application/x-www-form-urlencoded; charset=UTF-8";

REFacilityInfoCollector::REFacilityInfoCollector(QObject *parent)
    : REFacilityInfoCollector(Config::Default(), parent) {}

REFacilityInfoCollector::REFacilityInfoCollector(const Config &config,
                                                 QObject *parent)
    : QObject(parent),
      m_config(config),
      m_manager(new QNetworkAccessManager(this)),
      m_tokenRX(
          "<meta\\s+name='csrf-token-value'\\s+content='([a-z0-9]+)'\\s*\\/>",
          QRegularExpression::CaseInsensitiveOption),
      m_infoRowRX("<td>(.+?)<\\/td>\\s*<td>(.*?)<\\/td>",
                  QRegularExpression::CaseInsensitiveOption)

{
  connect(m_manager.get(), &QNetworkAccessManager::finished, this,
          &REFacilityInfoCollector::requestFinished);
}

REFacilityInfoCollector::~REFacilityInfoCollector() {}

void REFacilityInfoCollector::setConfig(const Config &config) {
  m_config = config;
}

void REFacilityInfoCollector::authorize() {
  auto context = REFacilityCollectorContext{true};

  auto request = QNetworkRequest(m_config.CheckURL);
  request.setAttribute(
      static_cast<QNetworkRequest::Attribute>(ContextAttribute),
      QVariant::fromValue(context));
  request.setHeader(QNetworkRequest::UserAgentHeader, AppName);

  m_manager->get(request);
}

void REFacilityInfoCollector::collect(REFacility reFacility, REFacilityID id) {
  if (reFacility.INN == "") {
    reFacility.INN = m_config.getINN();
  }

  if (reFacility.OGRN == "") {
    reFacility.OGRN = m_config.getOGRN();
  }

  QUrlQuery postData;
  postData.addQueryItem(QueryArgINN, reFacility.INN);
  postData.addQueryItem(QueryArgOGRN, reFacility.OGRN);
  postData.addQueryItem(QueryArgSerial, reFacility.Series);
  postData.addQueryItem(QueryArgNumber, reFacility.Number);
  postData.addQueryItem(QueryArgToken, m_token);

  auto context = REFacilityCollectorContext{false, reFacility, id};

  auto request = QNetworkRequest(m_config.CheckURL);
  request.setAttribute(
      static_cast<QNetworkRequest::Attribute>(ContextAttribute),
      QVariant::fromValue(context));
  request.setHeader(QNetworkRequest::UserAgentHeader, AppName);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    ContentTypeFormUrlEncoded);

  m_manager->post(request, postData.toString(QUrl::FullyEncoded).toUtf8());
}

void REFacilityInfoCollector::raiseError(const REFacilityInfoError &err,
                                         REFacilityID id) {
  qCritical() << "raised error:" << err.Message;

  emit error(id, err);
}

void REFacilityInfoCollector::authorizeRequestFinished(QNetworkReply *reply) {
  auto data = reply->readAll();

  if (auto match = m_tokenRX.match(data); match.hasMatch()) {
    m_token = match.captured(1);

    qDebug() << "found token:" << m_token;

    emit authorized();

    return;
  }

  if (m_config.HTTPProxy.InUse) {
    emit authorized();  // under bproxy, where is no token
  } else {
    raiseError(REFacilityInfoError{REFacilityInfoError::Authorization,
                                   "token not found"});
  }
}

void REFacilityInfoCollector::collectRequestFinished(
    const REFacilityCollectorContext &ctx, QNetworkReply *reply) {
  REFacilityInfo reFacilityInfo{ctx.REFacility};

  auto data = reply->readAll();

  QJsonDocument jsonResponse = QJsonDocument::fromJson(data);
  QJsonObject jsonObject = jsonResponse.object();

  if (auto msg = jsonObject[JsonResponseKeyMessage].toString(); msg != "") {
    return raiseError(REFacilityInfoError{REFacilityInfoError::Collect,
                                          QString("server error: %1").arg(msg)},
                      ctx.ID);
  }

  QString html = jsonObject[JsonResponseKeyHtml].toString();

  for (auto &it : m_infoRowRX.globalMatch(html)) {
    const auto &param = it.captured(1);
    const auto &value = it.captured(2);

    if (param == TableParamType)
      reFacilityInfo.Type = value;
    else if (param == TableParamStatus)
      reFacilityInfo.Status = value;
    else if (param == TableParamActiveFrom)
      reFacilityInfo.ActiveFromDate = value;
    else if (param == TableParamActiveTo)
      reFacilityInfo.ActiveToDate = value;
    else if (param == TableParamDateOfExclusion)
      reFacilityInfo.DateOfExclusion = value;
  }

  emit collected(ctx.ID, reFacilityInfo);
}

void REFacilityInfoCollector::requestFinished(QNetworkReply *reply) {
  reply->deleteLater();

  auto attr = reply->request().attribute(
      static_cast<QNetworkRequest::Attribute>(ContextAttribute));
  auto context = qvariant_cast<REFacilityCollectorContext>(attr);

  // error
  if (reply->error() != QNetworkReply::NoError) {
    auto code =
        reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString();
    auto reason =
        reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
    auto message = reply->readAll();
    auto errMsg =
        QString("http error: [%1:%2] \"%3\"").arg(code, reason, message);

    if (context.IsAuthorization) {
      return raiseError(
          REFacilityInfoError{REFacilityInfoError::Authorization, errMsg});
    }

    return raiseError(REFacilityInfoError{REFacilityInfoError::Collect, errMsg},
                      context.ID);
  }

  if (context.IsAuthorization) {
    return authorizeRequestFinished(reply);
  }

  return collectRequestFinished(context, reply);
}
