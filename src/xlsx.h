#ifndef XLSX_H
#define XLSX_H

#include <QString>
#include <QVector>

#include "refacilityinfo.h"

namespace xlsx {
QVector<REFacilityInfo> parseREFacilityInfo(const QString &fileName,
                                            int rowOffset = 1);

void writeREFacilityInfo(const QVector<REFacilityInfo> &infos,
                         const QString &fileName);
}  // namespace xlsx

#endif  // XLSX_H