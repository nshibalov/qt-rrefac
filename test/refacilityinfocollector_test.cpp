#include "refacilityinfocollector.h"

#include <gtest/gtest.h>

#include <QCoreApplication>
#include <QEventLoop>
#include <QObject>

#include "config.h"

TEST(REFacilityInfoCollectorTest, BasicTest) {
  int argc = 0;
  char **argv = 0;

  QCoreApplication app{argc, argv};

  int counter = 5;

  auto collector = REFacilityInfoCollector(Config::Default());

  QObject::connect(
      &collector, &REFacilityInfoCollector::authorized, &collector,
      [&collector]() {
        collector.collect(REFacility{DefaultINN, DefaultOGRN, "77 18", "19488"},
                          42);
        collector.collect(REFacility{DefaultINN, DefaultOGRN, "77 18", "19489"},
                          43);
        collector.collect(REFacility{DefaultINN, DefaultOGRN, "77 18", "19490"},
                          44);
        collector.collect(REFacility{DefaultINN, DefaultOGRN, "77 18", "19491"},
                          45);
        collector.collect(REFacility{DefaultINN, DefaultOGRN, "77 18", "41422"},
                          46);
      });

  QObject::connect(&collector, &REFacilityInfoCollector::collected, &collector,
                   [&app, &counter](REFacilityID id, REFacilityInfo info) {
                     qDebug()
                         << "received [id: " << id << "], type: " << info.Type
                         << ", status: " << info.Status;
                     if (--counter == 0) app.exit(0);
                   });

  QObject::connect(&collector, &REFacilityInfoCollector::error, &collector,
                   [&app, &counter](REFacilityID id, REFacilityInfoError err) {
                     if (err.Type != REFacilityInfoError::ErrorType::Collect)
                       return;

                     qDebug() << "error [id: " << id
                              << "], message: " << err.Message;

                     if (--counter == 0) app.exit(0);
                   });

  collector.authorize();

  app.exec();
}
